import {TYPE_ACTION} from './actions';

let default_state = {
    name: '',
    updateUser:{},
    registerForm:[]
}

// reducer
const reducer = (state = default_state, action) => {
    switch (action.type) {
        case TYPE_ACTION.UPDATE_USER:
            return {
                ...state,
                updateUser: action.value
            }
        case TYPE_ACTION.SET_ON_SUBMIT_FORM:
            return  {
                ...state,
                registerForm: action.value
            }
        case TYPE_ACTION.NAME:
            return {
                ...state,
                name: action.value
            }
        
        default:
            return state
    }
};

export default reducer;