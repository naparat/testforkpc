
export const TYPE_ACTION = {
    NAME: 'NAME',
    UPDATE_USER: 'UPDATE_USER',
    SET_ON_SUBMIT_FORM: 'SET_ON_SUBMIT_FORM'
}

export const updateUser = (value) => {
    return { type: TYPE_ACTION.UPDATE_USER, value: value }
}
export const setName = (value) => {
    return { type: TYPE_ACTION.NAME, value: value }
}
export const setOnSubmitForm = (registerForm) => {
	return {
		type: TYPE_ACTION.SET_ON_SUBMIT_FORM,
		value: registerForm
	}
}
