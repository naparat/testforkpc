import React, { Component } from 'react'
import './input.css'
import Label from '../label/Label';
import Validation from '../validate/Validation'

const Input = (props) => {
    return (
        <div className={"input " + props.class}>
            <Label 
                name={props.name}
                title={props.title}
                requireFill={props.requireFill}
            />
            <input
                className="input__box"
                name={props.name}
                type={props.type}
                value={props.value}
                onChange={props.handleChange}
                placeholder={props.placeholder} 
                require={props.requireFill && 'required'}
            />
            {props.requireFill ? <Validation validationText={props.validationText} />: null}
        </div>
    );
}

export default Input;