import React from 'react'
import './validation.css'

const Validation = (props) => {
    return (
        <div className={"validation__text "}>
            {props.validationText}
        </div>
    );
}

export default Validation;
