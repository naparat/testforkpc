import React from 'react'
import './select.css'
import Label from '../label/Label';
import Validation from '../validate/Validation'

const Select = (props) => {
    return (
        <div className={"select " + props.class}>
            <Label 
                name={props.name}
                title={props.title}
                requireFill={props.requireFill}
            />
            <select
                className="select__list"
                name={props.name}
                value={props.value}
                onChange={props.handleChange}
                required={props.requireFill && 'required'}
                >
                <option value="">{props.placeholder}</option>
                {props.options.map(option => {
                    return (
                        <option
                            key={option}
                            value={option}
                            label={option}>{option}
                        </option>
                    );
                })}
            </select>
            {props.requireFill ? <Validation validationText={props.validationText} />: null}
        </div>
    );
}

export default Select;
