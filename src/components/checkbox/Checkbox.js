import React from 'react'
import './checkbox.css'
const Checkbox = (props) => {
    return (
        <div className={"checkbox " + props.class}>
            <input type="checkbox" name={props.name} value={props.value} onChange={props.handleChange} />
        </div>
    );
}

export default Checkbox;
