import React, { Component } from 'react'
import './label.css'

const Label = (props) => {
    return (
        <label 
            htmlFor={props.name} 
            className="input__label">
            {props.title} 
            {props.requireFill && <span className="register__require"> * </span>}
        </label>
    );
}

export default Label;