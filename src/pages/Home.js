import React from 'react';

import './../styles/index.css';
import HomeContainer from '../containers/HomeContainer';

export class Home extends React.Component {
    render() {
        return (
            <div>
                <HomeContainer />
            </div>
        )
    }
}
