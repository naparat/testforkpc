import React from "react"
import { connect } from "react-redux"
import DatePicker from 'react-date-picker';
import MaskedInput from 'react-text-mask'
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'

import Input from "../components/input/Input"
import Select from '../components/select/Select'

import {setOnSubmitForm} from '../store/actions' 
import Label from "../components/label/Label";
import {nationalityList} from '../constants/Nationality'
import Validation from "../components/validate/Validation";

class HomeContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title:'',
            firstname:'',
            lastname:'',
            birthday:new Date(),
            nationality:'',
            citizen_id:'',
            phone:'',
            gender:'',
            passport_no:'',
            expect_salary:'',
            titleOptions:['Mr.' , 'Mrs.' , 'Miss'],
            register:[],
            value:'',
            requireFill:false,
            checkedToDelete:false,
            editing:false,
            titleError:'',
            firstnameError:'',
            lastnameError:'',
            birthdateError:'',
            phoneError:'',
            expect_salaryError:''

        }
    }
    onChangeDate = date => this.setState({ date })

    handleChangeInput(e) {
        let value = e.target.value;
        let name = e.target.name;
        this.setState({
            [name]: value
        })
    }

    
    onDelete = (e,index) => {
        e.preventDefault();
        if(window.confirm('Confirm To Delete ?')){
            this.props.registerForm.splice(index,1);
            this.setState({
                register:this.props.register
            })
            window.localStorage.register = JSON.stringify(this.props.registerForm);
        }
    }
   
    onEdit = (item,index) => {
        this.state.title = item.title;
        this.state.firstname = item.firstname;
        this.state.lastname = item.lastname;
        this.state.birthday = item.birthday;
        this.state.nationality = item.nationality;
        this.state.citizen_id = item.citizen_id;
        this.state.gender = item.gender;
        this.state.phone = item.phone;
        this.state.passport_no = item.passport_no;
        this.state.expect_salary = item.expect_salary;
        this.state.register.splice(index, 1);
        this.setState(this.state);

    }

    onEdit(index,value){
        const todoData = JSON.parse(window.localStorage.getItem('key'));
        todoData[index] = value;
        this.setState({todoList:todoData})
        window.localStorage.setItem("key", JSON.stringify(todoData));
      }

    validate = () => {
        let titleError = '';
        let firstnameError = '';
        let lastnameError = '';
        let birthdateError = '';
        let phoneError = '';
        let expect_salaryError = '';
        if(!this.state.title) {
            titleError = 'Please select your title'
        }
        if(!this.state.firstname) {
            firstnameError = 'Plese enter your Firstname'
        }
        if(!this.state.lastname) {
            lastnameError = 'Plese enter your Lastname'
        }
        if(!this.state.birthday) {
            birthdateError = 'Please select your birth date'
        }
        if(!this.state.phone) {
            phoneError = 'Please enter your phone number'
        }
        if(!this.state.expect_salary) {
            expect_salaryError = 'Please enter your expected salary'
        }
        if(titleError || firstnameError || lastnameError || birthdateError || phoneError || expect_salaryError) {
            this.setState({
                titleError,
                firstnameError,
                lastnameError,
                birthdateError,
                phoneError,
                expect_salaryError
            })
            return false
        }
        return true;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const isValid = this.validate();
        if(isValid) {
            this.setState({
                titleError:'',
                firstnameError:'',
                lastnameError:'',
                birthdateError:'',
                phoneError:'',
                expect_salaryError:''
            })
           const formData = {
                title:this.state.title,
                firstname:this.state.firstname,
                lastname:this.state.lastname,
                birthday:this.state.birthday,
                nationality:this.state.nationality,
                citizen_id:this.state.citizen_id,
                gender:this.state.gender,
                phone:this.state.phone,
                passport_no:this.state.passport_no,
                expect_salary:this.state.expect_salary,
            }
            this.props.registerForm.push(formData);
            this.setState({
                register: this.props.registerForm
            })
            this.props.setOnSubmitForm(this.props.registerForm)
            localStorage.setItem('register', JSON.stringify(this.props.registerForm))
            this.setState({
                title:'',
                firstname:'',
                lastname:'',
                birthday:new Date(),
                nationality:'',
                citizen_id:'',
                phone:'',
                gender:'',
                passport_no:'',
                expect_salary:'',
            })
        }
    }
  

    render() {
        return (
            <div className="register">
                <div className="register__blog">
                    <form className="register__form" name="form_regis" id="form_register" onSubmit={this.handleSubmit.bind(this)}>
                        <div className="register__row">
                            <Select 
                                name="title" 
                                title="Title" 
                                requireFill={true}
                                class={"register__inline"}
                                placeholder = {'Select Title'} 
                                handleChange = {this.handleChangeInput.bind(this)}
                                value={this.state.title}
                                validationText={this.state.titleError}
                                options = {this.state.titleOptions} />
                                
                            <Input 
                                name="firstname"
                                type="text" 
                                title="First name" 
                                requireFill={true}
                                value={this.state.firstname}
                                validationText={this.state.firstnameError}
                                handleChange = {this.handleChangeInput.bind(this)}
                                class={"register__inline"} />
                            <Input 
                                name="lastname" 
                                type="text" 
                                title="Last name" 
                                requireFill={true}
                                handleChange = {this.handleChangeInput.bind(this)}
                                value={this.state.lastname}
                                validationText={this.state.lastnameError}
                                class={"register__inline"} />
                        </div>
                        <div className="register__rows">
                            <div className="register__inline">
                                <Label 
                                    name={"birthdate"}
                                    title={"Birthday"}
                                    requireFill={true}
                                />
                                <DatePicker
                                    onChange={this.onChangeDate}
                                    locale="en-EN"
                                    clearIcon=""
                                    required={true}
                                    value={this.state.birthday}
                                    className={"register__date input__box"}
                                    />
                                <Validation validationText={this.state.birthdateError} />
                            </div>
                        <Select 
                            name="nationality" 
                            title="Nationality" 
                            class={"register__inline"}
                            placeholder = {'Nationality'} 
                            value={this.state.nationality}
                            handleChange = {this.handleChangeInput.bind(this)}
                            options = {nationalityList} />
                        </div>
                        <div className="register__rows">
                            <Label 
                                name={"citizen_id"}
                                title={"Citizen ID:"}
                                requireFill={false}
                            />
                            <MaskedInput
                                mask={[ /[0-9]/,'-',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'-', /[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'-',/[0-9]/,/[0-9]/,'-',/[0-9]/]}
                                className="input__box"
                                placeholder="Citizen ID"
                                name={'citizen_id'}
                                guide={true}
                                id="my-input-id"
                                onBlur={() => {}}
                                onChange={this.handleChangeInput.bind(this)}
                                />
                        </div>
                        <div className="register__rows">
                            <div className="register__inline">
                                <Label 
                                    name={"phone"}
                                    title={"Mobile Phone:"}
                                    requireFill={true}
                                />
                                <PhoneInput
                                    placeholder="Enter phone number"
                                    className={'register__inline'}
                                    value={ this.state.phone }
                                    onChange={ phone => this.setState({ phone }) } />
                                <Validation validationText={this.state.phoneError} />
                            </div>
                            
                        </div>
                        <div className="register__rows">
                            <Label 
                                name={"gender"}
                                title={"Gender"}
                                class={'register__inline'}
                            />
                            <Input 
                                name={"gender"}
                                value={"male"}
                                handleChange = {this.handleChangeInput.bind(this)}
                                type={"radio"}
                                class={'register__inline'}
                            />
                            <Label 
                                name={"gender"}
                                title={"Male"}
                                class={'register__inline'}
                            />
                             <Input 
                                name={"gender"}
                                value={"female"}
                                handleChange = {this.handleChangeInput.bind(this)}
                                type={"radio"}
                                class={'register__inline'}
                            />
                            <Label 
                                name={"gender"}
                                title={"Female"}
                                class={'register__inline'}
                            />
                            <Input 
                                name={"gender"}
                                value={"unisex"}
                                handleChange = {this.handleChangeInput.bind(this)}
                                type={"radio"}
                                class={'register__inline'}
                            />
                            <Label 
                                name={"gender"}
                                title={"Unisex"}
                                class={'register__inline'}
                            />
                        </div>
                        <div className="register__rows">
                            <Input 
                                name="passport_no"
                                type="text" 
                                title="Passport No." 
                                handleChange = {this.handleChangeInput.bind(this)}
                                value={this.state.passport_no}
                                />
                        </div>
                        <div className="register__rows">
                            <Input 
                                name="expect_salary" 
                                type="text" 
                                title="Expected Salary:" 
                                requireFill={true}
                                handleChange = {this.handleChangeInput.bind(this)}
                                value={this.state.expect_salary}
                                validationText={this.state.expect_salaryError} />
                        </div>
                        <button className="register-btns" type="submit" onClick={(e)=>this.handleSubmit(e)}>Submit</button>
                    </form>
                </div>
                <div className="register__info">
                    <div className="register__blog">
                        <table className="register-table">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>Name</td>
                                    <td>Gender</td>
                                    <td>Mobile Phone</td>
                                    <td>Nationality</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.registerForm.length > 0 && 
                                    this.props.registerForm.map((item,index) => {
                                    return (
                                        <tr key={item.firstname + index}>
                                            <td>
                                                {/* <input 
                                                    className={'register__inline'} 
                                                    type="checkbox" 
                                                    name="selectToDelete" 
                                                    value={index} 
                                                    handleChange = {this.handleChangeInput.bind(this)} /> */}
                                            </td>
                                            <td>{item.firstname}</td>
                                            <td>{item.gender}</td>
                                            <td>{item.phone}</td>
                                            <td>{item.nationality}</td>
                                            <td>
                                                <a className="btns__inline" onClick={() => this.onEdit(item,index)}>EDIT</a>
                                                <a className="btns__inline" onClick={(e) => this.onDelete(e,index)}>DELETE</a>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    name: state.Reducer.name,
    updateUser: state.Reducer.updateUser,
    registerForm: state.Reducer.registerForm
})

const mapDispatchToProps = {
    setOnSubmitForm
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer);
