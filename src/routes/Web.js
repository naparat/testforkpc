import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import { Home } from '../pages/Home';

export class Web extends Component {
	render() {
		return (
			<div>
				<Route path={"/"} exact component={Home} />
			</div>
		);
	}
}
