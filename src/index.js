import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import {combineReducers, createStore} from 'redux';
import {Web} from './routes/Web';

import Reducer from './store/reducer';

const store = createStore(
	combineReducers({
		Reducer
	})
);
ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Web />
        </BrowserRouter>
    </Provider>,
	document.getElementById('root')
)
